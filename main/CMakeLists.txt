
add_executable(gpgpass)
target_link_libraries(gpgpass gpgpass_internal KF${QT_MAJOR_VERSION}::IconThemes)
if (QT_MAJOR_VERSION STREQUAL "6")
    target_link_libraries(gpgpass KF${QT_MAJOR_VERSION}::ColorScheme KPim6::Libkleo)
endif()

ecm_add_app_icon(GPGPASS_ICONS ICONS ${CMAKE_SOURCE_DIR}/artwork/sc-gpgpass.svg ${CMAKE_SOURCE_DIR}/artwork/32-gpgpass.png ${CMAKE_SOURCE_DIR}/artwork/64-gpgpass.png ${CMAKE_SOURCE_DIR}/artwork/256-gpgpass.png)
target_sources(gpgpass PRIVATE
    main.cpp
    aboutdata.cpp
    gpgpass.qrc
    ${GPGPASS_ICONS}
)
target_compile_definitions(gpgpass PRIVATE QT_NO_TRANSLATION)

if(WIN32)
    configure_file(versioninfo.rc.in versioninfo.rc)
    configure_file(gpgpass.w32-manifest.in gpgpass.w32-manifest)
    target_sources(gpgpass PRIVATE
        ${CMAKE_CURRENT_BINARY_DIR}/versioninfo.rc
        ${CMAKE_CURRENT_BINARY_DIR}/gpgpass.w32-manifest
    )
endif()

install(TARGETS gpgpass ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
