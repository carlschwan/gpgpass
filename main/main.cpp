/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/

#include "aboutdata.h"
#include "widgets/mainwindow.h"

#include <QApplication>

#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
#include <KColorSchemeManager>
#endif
#include <KIconLoader>
#include <KLocalizedString>
#include <gpgpass_version.h>

#ifdef Q_OS_WINDOWS
#include <windows.h>
#endif

#include <sys/stat.h>
int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
#endif
// allow darkmode
#if defined(Q_OS_WIN)
    if (!qEnvironmentVariableIsSet("QT_QPA_PLATFORM")) {
        qputenv("QT_QPA_PLATFORM", "windows:darkmode=1");
    }
#endif

    QApplication app(argc, argv);

    KLocalizedString::setApplicationDomain("gpgpass");

    AboutData aboutData;
    KAboutData::setApplicationData(aboutData);

    Q_INIT_RESOURCE(resources);

#ifdef Q_OS_WINDOWS
    if (AttachConsole(ATTACH_PARENT_PROCESS)) {
        freopen("CONOUT$", "w", stdout);
        freopen("CONOUT$", "w", stderr);
    }
    QApplication::setWindowIcon(QIcon(QStringLiteral(":/artwork/64-gpgpass.png")));
#else
    QApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("org.gnupg.gpgpass")));
#endif
    auto w = new MainWindow;

    // ensure KIconThemes is loaded for rcc icons
    KIconLoader::global()->hasIcon(QString{});

#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
    KColorSchemeManager m;
#endif

    w->show();

    return app.exec();
}
