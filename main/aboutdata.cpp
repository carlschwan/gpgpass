/*
    aboutdata.cpp

    This file is part of Kleopatra, the KDE keymanager
    SPDX-FileCopyrightText: 2001, 2002, 2004 Klarälvdalens Datakonsult AB

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include <gpgpass_version.h>

#include "aboutdata.h"

#include <Libkleo/GnuPG>
#include <libkleo_version.h>

#include <QCoreApplication>
#include <QSettings>
#include <QThread>

#include <KLazyLocalizedString>
#include <KLocalizedString>
#include <kcoreaddons_version.h>

struct about_data {
    const KLazyLocalizedString name;
    const KLazyLocalizedString description;
};

static constexpr auto authors = std::to_array<about_data>({
    {kli18n("Carl Schwan"), kli18n("Maintainer")},
    {kli18n("Sune Vuorela"), kli18n("Developer")},
});

static constexpr auto credits = std::to_array<about_data>({
    {kli18n("Anne Jan Brouwer"), kli18n("QtPass developer")},
});

// Extend the about data with the used GnuPG Version since this can
// make a big difference with regards to the available features.
static void loadBackendVersions()
{
#if LIBKLEO_VERSION >= QT_VERSION_CHECK(6, 3, 41)
    auto thread = QThread::create([]() {
        const auto backendComponents = Kleo::backendComponents();
        if (!backendComponents.empty()) {
            QMetaObject::invokeMethod(qApp, [backendComponents]() {
                auto about = KAboutData::applicationData();
                for (const auto &component : backendComponents) {
#if KCOREADDONS_VERSION >= QT_VERSION_CHECK(6, 9, 0)
                    about.addComponent(component);
#else
                    about.addComponent(component.name(), component.description(), component.version(), component.webAddress() /*, component.license() */);
#endif
                }
                KAboutData::setApplicationData(about);
            });
        }
    });
    thread->start();
#endif
}

AboutData::AboutData()
    : KAboutData(QStringLiteral("gpgpass"),
                 i18n("GPGPass"),
                 QLatin1String(GPGPASS_VERSION_STRING),
                 i18nc("@info", "Password Manager using GnuPG to store your password securely."),
                 KAboutLicense::GPL,
                 i18nc("@info:credit", "\u00A9 2023-%1 g10 Code GmbH", QStringLiteral("2024")) + QLatin1Char('\n')
                     + i18nc("@info:credit", "\u00A9 2014-2023 Anne Jan Brouwer") + QLatin1Char('\n'),
                 {},
                 QStringLiteral("https://www.gnupg.com/"),
                 QStringLiteral("https://dev.gnupg.org/maniphest/task/edit/form/3/"))
{
    setOrganizationDomain("gnupg.org");
    setDesktopFileName(QStringLiteral("org.gnupg.gpgpass"));
    using ::authors;
    using ::credits;

    for (const auto &author : authors) {
        addAuthor(author.name.toString(), author.description.toString());
    }

    for (const auto &credit : credits) {
        addCredit(credit.name.toString(), credit.description.toString());
    }

    loadBackendVersions();
}
