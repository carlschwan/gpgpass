// SPDX-FileCopyrightText: 2018 KeePassXC Team <team@keepassxc.org>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#pragma once

#include "passwordgenerator.h"
#include <QObject>

class TestPasswordGenerator : public QObject
{
    Q_OBJECT

private:
    PasswordGenerator m_generator;

private Q_SLOTS:
    void initTestCase();
    void init();

    void testCustomCharacterSet_data();
    void testCustomCharacterSet();
    void testCharClasses_data();
    void testCharClasses();
    void testLookalikeExclusion_data();
    void testLookalikeExclusion();
    void testMinLength_data();
    void testMinLength();
    void testValidity_data();
    void testValidity();
    void testReset();
};
