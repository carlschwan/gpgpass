// SPDX-FileCopyrightText: 2019 KeePassXC Team <team@keepassxc.org>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#include "tst_passphrasegenerator.h"
#include "passphrasegenerator.h"

#include <QRegularExpression>
#include <QTest>

QTEST_GUILESS_MAIN(TestPassphraseGenerator)

void TestPassphraseGenerator::initTestCase()
{
    Q_INIT_RESOURCE(resources);
}

void TestPassphraseGenerator::testWordCase()
{
    PassphraseGenerator generator;
    generator.setWordSeparator(QStringLiteral(" "));
    QVERIFY(generator.isValid());

    QString passphrase;
    passphrase = generator.generatePassphrase();
    QCOMPARE(passphrase, passphrase.toLower());

    generator.setWordCase(PassphraseGenerator::LOWERCASE);
    passphrase = generator.generatePassphrase();
    QCOMPARE(passphrase, passphrase.toLower());

    generator.setWordCase(PassphraseGenerator::UPPERCASE);
    passphrase = generator.generatePassphrase();
    QCOMPARE(passphrase, passphrase.toUpper());

    generator.setWordCase(PassphraseGenerator::TITLECASE);
    passphrase = generator.generatePassphrase();
    QRegularExpression regex(QStringLiteral("^([A-Z][a-z]* ?)+$"));
    QVERIFY(regex.match(passphrase).hasMatch());
}
