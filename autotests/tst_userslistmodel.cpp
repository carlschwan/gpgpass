// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "models/userslistmodel.h"
#include "setupenv.h"
#include <QAbstractItemModelTester>
#include <QObject>
#include <QSignalSpy>
#include <QTest>

class UsersListModelTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void testModel();
};

void UsersListModelTest::initTestCase()
{
    Test::setupEnv();
}

void UsersListModelTest::testModel()
{
    auto model = new UsersListModel({"14B79E26050467AA", "02325448204E452A"}, this);
    auto tester = new QAbstractItemModelTester(model);
    Q_UNUSED(tester);
    QSignalSpy spy(model, &UsersListModel::finishedKeysFetching);
    spy.wait();
    QCOMPARE(model->rowCount({}), 6);

    // valid key with secrets
    QCOMPARE(model->data(model->index(0, 0), Qt::DisplayRole).toString(),
             QStringLiteral("unittest key (no password) <test@kolab.org>\n8D9860C58F246DE6 created 11/13/09 5:33\u202FPM"));
    QCOMPARE(model->data(model->index(0, 0), UsersListModel::NameRole).toString(), QStringLiteral("unittest key (no password) <test@kolab.org>"));
    QCOMPARE(model->data(model->index(0, 0), Qt::CheckStateRole).value<Qt::CheckState>(), Qt::Unchecked);
    QCOMPARE(model->data(model->index(0, 0), Qt::UserRole).value<UserInfo>().have_secret, true);
    QCOMPARE(model->data(model->index(0, 0), Qt::ForegroundRole).value<QColor>(), Qt::blue);
    QCOMPARE(model->data(model->index(0, 0), Qt::FontRole).value<QFont>().bold(), 1);

    // no secret
    QCOMPARE(model->data(model->index(2, 0), UsersListModel::NameRole).toString(), QStringLiteral("Leonardo Franchi (lfranchi) <lfranchi@kde.org>"));
    QCOMPARE(model->data(model->index(2, 0), Qt::UserRole).value<UserInfo>().have_secret, false);
    QCOMPARE(model->data(model->index(2, 0), Qt::ForegroundRole).value<QColor>(), Qt::white);

    // key not found in keyring
    QCOMPARE(model->index(5, 0).data(UsersListModel::NameRole).toString(), QStringLiteral(" ?? Key not found in keyring"));
}

QTEST_MAIN(UsersListModelTest)
#include "tst_userslistmodel.moc"