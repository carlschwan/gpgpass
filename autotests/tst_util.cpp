/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#include "util.h"
#include <QTest>

class UtilTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void normalizedFolderPath()
    {
        QCOMPARE(Util::normalizeFolderPath(QStringLiteral("test")), QStringLiteral("test/"));
        QCOMPARE(Util::normalizeFolderPath(QStringLiteral("test/")), QStringLiteral("test/"));
    }
};
QTEST_MAIN(UtilTest)
#include "tst_util.moc"
