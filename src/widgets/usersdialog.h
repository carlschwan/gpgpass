/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef USERSDIALOG_H_
#define USERSDIALOG_H_

#include <QDialog>
#include <QList>

class Ui_UsersDialog;
class Ui_UsersWidget;
class UsersListModel;
class FilterUsersListModel;

class QCloseEvent;
class QKeyEvent;
class QListWidgetItem;

/*!
    \class UsersDialog
    \brief Handles listing and editing of GPG users.

    Selection of whom to encrypt to.
 */
class UsersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UsersDialog(const QList<QByteArray> &recipients, QWidget *parent = nullptr);
    ~UsersDialog();

    void showError(const QString &errorText);

public Q_SLOTS:
    void accept() override;

Q_SIGNALS:
    void save(const QList<QByteArray> &recipients);

private:
    std::unique_ptr<Ui_UsersDialog> dialogUi;
    std::unique_ptr<Ui_UsersWidget> ui;

    UsersListModel *const m_usersListModel;
    FilterUsersListModel *const m_filterUsersListModel;

    void setUi(Ui_UsersWidget *ui);
    void itemChange(QListWidgetItem *item);
};

#endif // USERSDIALOG_H_
