// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QLineEdit>
#include <QWidget>

#include <KUrlRequester>
#include <Libkleo/KeySelectionCombo>
#include <qpushbutton.h>

#include "formtextinput.h"

class SetupWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SetupWidget(QWidget *parent = nullptr);
    ~SetupWidget();

Q_SIGNALS:
    void setupComplete(const QString &location, const QByteArray &keyId);

private:
    void slotAccepted();
    void slotUpdateSaveButtonState();
    void slotUpdateLocationHint();

    struct {
        QWidget *wrapperWidget = nullptr;
        std::unique_ptr<Kleo::FormTextInput<KUrlRequester>> locationInput;
        std::unique_ptr<Kleo::FormTextInput<Kleo::KeySelectionCombo>> keySelectionInput;
        QPushButton *saveButton = nullptr;
    } ui;
};