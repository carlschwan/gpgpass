// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-Contributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "welcomewidget.h"
#include "ui_welcomewidget.h"
#include <gpgpass_version.h>

WelcomeWidget::WelcomeWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::WelcomeWidget)
{
    ui->setupUi(this);

    // center vertically
    ui->mainLayout->insertStretch(0);
    ui->mainLayout->addStretch();

    const double titleFontSize = QApplication::font().pointSize() * 2;
    auto font = ui->title->font();
    font.setPointSize(titleFontSize);
    ui->title->setFont(font);

    QPixmap logo = QPixmap(QStringLiteral(":/artwork/64-gpgpass.png"));
    ui->logo->setPixmap(logo);

    ui->version->setText(i18nc("@info", "Version: %1", QString::fromLocal8Bit(GPGPASS_VERSION_STRING)));
}

WelcomeWidget::~WelcomeWidget() = default;
