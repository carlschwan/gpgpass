/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2016-2017 tezeb <tezeb+github@outoftheblue.pl>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2019 Maciej S. Szmigiero <mail@maciej.szmigiero.name>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include "conf/configuredialog.h"
#include "models/storemodel.h"

#include <KSelectionProxyModel>
#include <QFileSystemModel>
#include <QItemSelectionModel>
#include <QMainWindow>
#include <QProcess>
#include <QTimer>

#include <KXmlGuiWindow>

#ifdef __APPLE__
// http://doc.qt.io/qt-5/qkeysequence.html#qt_set_sequence_auto_mnemonic
void qt_set_sequence_auto_mnemonic(bool b);
#endif

namespace Ui
{
class MainWindow;
}

class QComboBox;
class ClipboardHelper;
class KMessageWidget;
class AddFileInfoProxy;
class KJob;
class PasswordViewerWidget;
class PasswordEditorWidget;
class RootFoldersManager;

/*!
    \class MainWindow
    \brief The MainWindow class does way too much, not only is it a switchboard,
    configuration handler and more, it's also the process-manager.

    This class could really do with an overhaul.
 */
class MainWindow : public KXmlGuiWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void restoreWindow();
    void userDialog(QString = {});

    /// Open the configuration dialog
    void openConfig(ConfigureDialog::Page page = ConfigureDialog::Page::None);

    void setUiElementsEnabled(bool state);

    const QModelIndex getCurrentTreeViewIndex();

    void setVisible(bool visible) override;

protected:
    void closeEvent(QCloseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void changeEvent(QEvent *event) override;
    bool eventFilter(QObject *obj, QEvent *event) override;

public Q_SLOTS:
    void deselect();

    void selectTreeItem(const QModelIndex &index);

private Q_SLOTS:
    void addPassword();
    void addFolder();
    void onEdit(bool edit);
    void onDelete();
    void onUsers();
    void editTreeItem(const QModelIndex &index);
    void switchToPasswordEditor(const QString &filePath, const QString &content = {});
    void filterList(const QString &arg1);
    void selectFromSearch();
    void showContextMenu(const QPoint &pos);
    void renameFolder();
    void renamePassword();
    void focusInput();
    void onTimeoutSearch();
    void verifyInitialized();
    void slotSetupFinished(const QString &location, const QByteArray &keyId);
    void updateRootIndex();

private:
    QString fallbackStore();

    QScopedPointer<Ui::MainWindow> ui;
    ClipboardHelper *const m_clipboardHelper;
    PasswordViewerWidget *const m_passwordViewer;
    PasswordEditorWidget *const m_passwordEditor;
    RootFoldersManager *const m_rootFoldersManager;
    StoreModel m_storeModel;
    QTimer searchTimer;
    KMessageWidget *m_notInitialized;
    KMessageWidget *m_errorMessage;
    QString m_selectedFile;
    bool firstShow = true;

    QAction *m_actionAddEntry = nullptr;
    QAction *m_actionAddFolder = nullptr;
    QAction *m_actionEdit = nullptr;
    QAction *m_actionDelete = nullptr;
    QAction *m_actionUsers = nullptr;
    QAction *m_actionConfig = nullptr;

    void initToolBarButtons();

    void updateText();
    void selectFirstFile();
    QModelIndex firstFile(QModelIndex parentIndex);
    void setPassword(QString, bool isNew = true);

    void initTrayIcon();
    void destroyTrayIcon();
    void reencryptPath(QString dir);
};

#endif // MAINWINDOW_H_
