// SPDX-FileCopyrightText: 2013 Felix Geyer <debfx@fobos.de>
// SPDX-FileCopyrightText: 2022 KeePassXC Team <team@keepassxc.org>
// SPDX-License-Identifier: GPL-2.0-only or GPL-3.0-only

#pragma once

#include <QComboBox>
#include <QTimer>

#include "clipboardhelper.h"
#include "passphrasegenerator.h"
#include "passwordgenerator.h"

namespace Ui
{
class PasswordGeneratorWidget;
}

class PasswordGenerator;
class PasswordHealth;
class PassphraseGenerator;

class PasswordGeneratorWidget : public QWidget
{
    Q_OBJECT

public:
    enum GeneratorTypes { Password = 0, Diceware = 1 };

    explicit PasswordGeneratorWidget(ClipboardHelper *clipboardHelper, QWidget *parent = nullptr);
    ~PasswordGeneratorWidget() override;

    void loadSettings();
    void saveSettings();
    void setPasswordLength(int length);
    void setStandaloneMode(bool standalone);
    QString getGeneratedPassword();
    bool isPasswordVisible() const;
    bool isPasswordGenerated() const;

    static PasswordGeneratorWidget *popupGenerator(ClipboardHelper *clipboardHelper, QWidget *parent = nullptr);

Q_SIGNALS:
    void appliedPassword(const QString &password);
    void closed();

public Q_SLOTS:
    void regeneratePassword();
    void applyPassword();
    void copyPassword();
    void setPasswordVisible(bool visible);
    void deleteWordList();
    void addWordList();

protected:
    void closeEvent(QCloseEvent *event) override;

private Q_SLOTS:
    void updateButtonsEnabled(const QString &password);
    void updatePasswordStrength();
    void setAdvancedMode(bool advanced);
    void excludeHexChars();

    void passwordLengthChanged(int length);
    void passphraseLengthChanged(int length);

    void updateGenerator();

private:
    ClipboardHelper *const m_clipboardHelper;
    bool m_standalone = false;
    bool m_passwordGenerated = false;
    int m_firstCustomWordlistIndex;

    PasswordGenerator::CharClasses charClasses();
    PasswordGenerator::GeneratorFlags generatorFlags();

    const QScopedPointer<PasswordGenerator> m_passwordGenerator;
    const QScopedPointer<PassphraseGenerator> m_dicewareGenerator;
    const QScopedPointer<Ui::PasswordGeneratorWidget> m_ui;
};
