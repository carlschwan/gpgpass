/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef PasswordEditorWidget_H_
#define PasswordEditorWidget_H_

#include "adjustingscrollarea.h"
#include "passentry.h"

namespace Ui
{
class PasswordEditorWidget;
}

class QLineEdit;
class ClipboardHelper;

/*!
    \class PasswordEditorWidget
    \brief PasswordEditorWidget Handles the inserting and editing of passwords.

    Includes templated views.
 */
class PasswordEditorWidget : public Kleo::AdjustingScrollArea
{
    Q_OBJECT

public:
    explicit PasswordEditorWidget(ClipboardHelper *clipboardHelper, QWidget *parent = nullptr);
    ~PasswordEditorWidget() override;

    PassEntry passEntry() const;
    void setPassEntry(const PassEntry &passEntry);

    void accept();

Q_SIGNALS:
    void save(const QString &content);
    void editorClosed();

private Q_SLOTS:
    void createPassword();

private:
    void load();

    QString toRawContent() const;

    std::unique_ptr<Ui::PasswordEditorWidget> ui;
    PassEntry m_passEntry;
    ClipboardHelper *const m_clipboardHelper;
    QStringList m_fields;
    QList<QLineEdit *> m_templateLines;
    QList<QLineEdit *> m_otherLines;
};

#endif // PasswordEditorWidget_H_
