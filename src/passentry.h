// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QList>
#include <QString>

/// This class represent a decrypted pass entry.
class PassEntry
{
public:
    PassEntry();

    explicit PassEntry(const QString &name, const QString &content);

    /// \param templateFields the fields in the template. Fields in the
    /// template will always be in namedValues() at the beginning of the
    /// list in the same order.
    ///
    /// \param allFields whether all fields should be considered as named
    /// values. If set to false only templateFields are returned in
    /// namedValues().
    explicit PassEntry(const QString &name, const QString &content, const QStringList &templateFields, bool allFields);

    struct NamedValue {
        QString name;
        QString value;
    };

    /// \brief The NamedValues class is mostly a list of NamedValue but also
    /// has a method to take a specific NamedValue pair out of the list.
    class NamedValues : public QList<NamedValue>
    {
    public:
        NamedValues();
        NamedValues(std::initializer_list<NamedValue> values);

        QString takeValue(const QString &name);
    };

    bool isNull() const;

    /// \return the name from the file name
    QString name() const;

    /// \return the password from the parsed file.
    QString password() const;

    /// \return the named values in the file in the order of appearence, with
    /// template values first.
    NamedValues namedValues() const;

    /// \return the data that is not the password and not in namedValues.
    QString remainingData() const;

    /// Same as remainingData but without data that should not be displayed
    /// (like a TOTP secret).
    QString remainingDataForDisplay() const;

private:
    QString m_name;
    QString m_password;
    QString m_remainingData;
    QString m_remainingDataForDisplay;
    NamedValues m_namedValues;
};

bool operator==(const PassEntry::NamedValue &a, const PassEntry::NamedValue &b);

QDebug operator<<(QDebug debug, const PassEntry::NamedValue &entry);
QDebug operator<<(QDebug d, const PassEntry &model);
