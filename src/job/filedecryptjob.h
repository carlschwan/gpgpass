// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KJob>

namespace GpgME
{
class DecryptionResult;
class Error;
}

namespace QGpgME
{
class DecryptJob;
}

class FileDecryptJob : public KJob
{
    Q_OBJECT

public:
    enum {
        AccessPermissionError = UserDefinedError,
        DecryptionError,
    };
    explicit FileDecryptJob(const QString &filePath, QObject *parent = nullptr);

    void start() override;

    QString content() const;
    QString filePath() const;

    /// The recipients for which the file was encrypted for.
    ///
    /// \note This list is sorted.
    QList<QByteArray> recipients() const;

private Q_SLOTS:
    void
    slotDecryptedResult(const GpgME::DecryptionResult &result, const QByteArray &plainText, const QString &auditLogAsHtml, const GpgME::Error &auditLogError);

private:
    QGpgME::DecryptJob *m_job = nullptr;
    QString m_filePath;
    QString m_content;
    QList<QByteArray> m_recipients;
};
