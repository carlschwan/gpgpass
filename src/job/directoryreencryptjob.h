// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include "userinfo.h"
#include <KCompositeJob>

class StoreModel;

/// Decrypt and then reencrypt recursively all files in a directory with new recipients
class DirectoryReencryptJob : public KCompositeJob
{
    Q_OBJECT

public:
    enum {
        FilePermissionError,
        IOError,
        NoSecretKeyError,
    };
    explicit DirectoryReencryptJob(const StoreModel &storeModel, const QList<QByteArray> &userKeys, const QString &directory, QObject *parent = nullptr);

    void start() override;

protected:
    void slotResult(KJob *job) override;

private:
    bool writeGpgIdList();

    QString m_directory;
    const StoreModel &m_storeModel;
    const QList<QByteArray> m_userKeys;
};
