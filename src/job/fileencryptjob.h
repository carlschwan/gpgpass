// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KJob>

namespace GpgME
{
class EncryptionResult;
class Error;
}

class FileEncryptJob : public KJob
{
    Q_OBJECT

public:
    enum {
        PermissionError = UserDefinedError,
        EncryptionError,
    };
    explicit FileEncryptJob(const QString &filePath, const QByteArray &content, const QList<QByteArray> &recipientsKeys, QObject *parent = nullptr);

    void start() override;

private Q_SLOTS:
    void
    slotEncryptedResult(const GpgME::EncryptionResult &result, const QByteArray &ciphertext, const QString &auditLogAsHtml, const GpgME::Error &auditLogError);

private:
    QString m_filePath;
    QByteArray m_content;
    QList<QByteArray> m_recipientsKeys;
};
