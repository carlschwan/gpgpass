/*
    SPDX-FileCopyrightText: 2024 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#include "addfileinfoproxy.h"
#include <QFileSystemModel>

AddFileInfoProxy::AddFileInfoProxy(QObject *parent)
    : QIdentityProxyModel{parent}
{
}

QVariant AddFileInfoProxy::data(const QModelIndex &index, int role) const
{
    if (!m_model) {
        return QVariant();
    }
    if (role == FileInfoRole) {
        auto source_index = mapToSource(index);
        return QVariant::fromValue(m_model->fileInfo(source_index));
    }
    return QIdentityProxyModel::data(index, role);
}

void AddFileInfoProxy::setSourceModel(QAbstractItemModel *newModel)
{
    m_model = qobject_cast<QFileSystemModel *>(newModel);
    QIdentityProxyModel::setSourceModel(m_model);
}
