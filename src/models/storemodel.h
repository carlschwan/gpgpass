/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2019 Maciej S. Szmigiero <mail@maciej.szmigiero.name>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef STOREMODEL_H_
#define STOREMODEL_H_

#include <QSortFilterProxyModel>

class QFileInfo;
class QFileSystemModel;
class QItemSelectionModel;
class KSelectionProxyModel;
class AddFileInfoProxy;
class Pass;
class RootFoldersManager;
class RootFolderConfig;

/// \brief The QSortFilterProxyModel for handling filesystem searches.
///
/// This model support drag and drop.
class StoreModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    explicit StoreModel(QObject *parent = nullptr);

    RootFoldersManager *rootFoldersManager() const;
    void setRootFoldersManager(RootFoldersManager *rootFoldersManager);

    /// Overriden to remove .gpg at the end of the file name.
    QVariant data(const QModelIndex &index, int role) const override;
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
    int columnCount(const QModelIndex &parent) const override;

    Qt::DropActions supportedDropActions() const override;
    Qt::DropActions supportedDragActions() const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QStringList mimeTypes() const override;
    QMimeData *mimeData(const QModelIndexList &indexes) const override;
    bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const override;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;

    /// \return The recipients for a file
    /// This list is created from the .gpg-id file in the same directory or parent
    /// directory and is sorted.
    QList<QByteArray> recipientsForFile(const QFileInfo &fileInfo) const;

    void move(const QString &source, const QString &destination, const bool force = false);
    void copy(const QString &source, const QString &destination, const bool force = false);

    QModelIndex indexForPath(const QString &filePath);

Q_SIGNALS:
    void errorOccurred(const QString &errorText);
    void directoryLoaded(const QString &path);
    void rootFoldersSizeChanged();

private:
    void updateRootFolders();

private:
    QFileSystemModel *const m_fileSystemModel;
    AddFileInfoProxy *const m_addRoleModel;
    QItemSelectionModel *const m_itemSelectionModel;
    KSelectionProxyModel *const m_selectionProxyModel;
    RootFoldersManager *m_rootFoldersManager = nullptr;
};

#endif // STOREMODEL_H_
