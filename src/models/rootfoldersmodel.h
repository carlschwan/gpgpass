// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "storemodel.h"

#include <QAbstractListModel>

class RootFoldersManager;

class RootFoldersModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit RootFoldersModel(RootFoldersManager *manager, QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    int rowCount(const QModelIndex &parent) const override;

    void load();
    QList<RootFolderConfig *> rootFolders() const;

    QModelIndex addNewFolder();
    void slotRemoveFolder(const QModelIndex &index);

private:
    RootFoldersManager *const m_manager;
    StoreModel m_storeModel;

    QList<RootFolderConfig *> m_rootFolders;
};
