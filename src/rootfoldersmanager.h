// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include "models/rootfoldersmodel.h"
#include "models/storemodel.h"
#include "rootfolderconfig.h"

#include <QObject>

class RootFoldersManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<RootFolderConfig *> rootFolders READ rootFolders NOTIFY rootFoldersChanged)

public:
    RootFoldersManager(QObject *parent = nullptr);

    QList<RootFolderConfig *> rootFolders() const;

    RootFolderConfig *addRootFolder(const QString &name, const QString &path);

    /// Mark a folder as deleted.
    ///
    /// Will be actually deleted when calling the save method, unless load is called.
    void markAsDeleted(RootFolderConfig *config);

    QString rootFolderName(const QString &dirName) const;

    void load();
    void save();

Q_SIGNALS:
    void rootFoldersChanged();
    void storeChanged(RootFolderConfig *store);

private:
    QList<RootFolderConfig *> m_rootFolders;
    QList<RootFolderConfig *> m_deletedRootFolders;
};
