// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "gpgpassconfigmodule.h"

namespace Ui
{
class GeneralConfigurationPage;
}

namespace GpgPass
{
namespace Config
{

class GeneralConfigurationPage : public GpgPassConfigModule
{
    Q_OBJECT
public:
    explicit GeneralConfigurationPage(QWidget *parent);

    void load() override;
    void save() override;
    void defaults() override;

    void setAutoClearPanelSubentries(bool autoclear);
    void setAutoClearSubentries(bool autoclear);
    void setAutoclearPanel(bool autoclear);
    void setAutoclear(bool autoclear);

private:
    Ui::GeneralConfigurationPage *const ui;
};
}
}
