/*
    SPDX-FileCopyrightText: 2016 Bundesamt für Sicherheit in der Informationstechnik
    SPDX-FileContributor: Intevation GmbH

    SPDX-License-Identifier: GPL-2.0-only
*/

#pragma once

#include <KPageDialog>
#include <QList>
#include <QSet>

class KPageWidgetItem;

namespace GpgPass
{
namespace Config
{
class GpgPassConfigModule;
}
}

class GpgPassPageConfigDialog : public KPageDialog
{
    Q_OBJECT
public:
    explicit GpgPassPageConfigDialog(QWidget *parent = nullptr);

    KPageWidgetItem *
    addModule(const QString &name, const QString &docPath, const QString &icon, const QList<QAction *> actions, GpgPass::Config::GpgPassConfigModule *module);

Q_SIGNALS:
    void configCommitted();

protected Q_SLOTS:
    void slotDefaultClicked();
    void slotUser1Clicked();
    void slotApplyClicked();
    void slotOkClicked();
    void slotCurrentPageChanged(KPageWidgetItem *current, KPageWidgetItem *previous);
    void moduleChanged(bool value);

private:
    void clientChanged();
    void apply();

    QList<GpgPass::Config::GpgPassConfigModule *> mModules;
    QSet<GpgPass::Config::GpgPassConfigModule *> mChangedModules;
    QMap<QString, QString> mHelpUrls;
};
