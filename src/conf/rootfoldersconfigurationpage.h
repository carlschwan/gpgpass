// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "gpgpassconfigmodule.h"
#include "models/rootfoldersmodel.h"
#include "widgets/formtextinput.h"

#include <KUrlRequester>
#include <Libkleo/KeySelectionCombo>
#include <QLineEdit>

class RootFoldersManager;
class RootFolderConfig;
class QCheckBox;

namespace Ui
{
class RootFoldersConfigurationPage;
}

namespace GpgPass
{
namespace Config
{

class RootFoldersConfigurationPage : public GpgPassConfigModule
{
    Q_OBJECT
public:
    explicit RootFoldersConfigurationPage(RootFoldersManager *rootFoldersManager, QAction *addStoreAction, QWidget *parent);

    void load() override;
    void save() override;
    void defaults() override;

private:
    RootFolderConfig *fromCurrentIndex();
    void selectTreeItem(const QModelIndex &index);
    void slotNameChanged(const QString &text);
    void slotLocationChanged(const QString &text);

    Ui::RootFoldersConfigurationPage *const ui;

    RootFoldersManager *const m_rootFoldersManager;
    RootFoldersModel *const m_rootFoldersModel;
    QAction *const m_addStoreAction;
    std::unique_ptr<Kleo::FormTextInput<QLineEdit>> m_nameInput;
    std::unique_ptr<Kleo::FormTextInput<KUrlRequester>> m_locationInput;
    std::unique_ptr<Kleo::FormTextInput<Kleo::KeySelectionCombo>> m_keySelectionInput;
};
}
}
