// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "gpgpassconfigmodule.h"

namespace Ui
{
class TemplateConfigurationPage;
}

namespace GpgPass
{
namespace Config
{

class TemplateConfigurationPage : public GpgPassConfigModule
{
    Q_OBJECT
public:
    explicit TemplateConfigurationPage(QWidget *parent);

    void load() override;
    void save() override;
    void defaults() override;

private:
    void toggleTemplateSubentries(bool enable);
    void useTemplate(bool useTemplate);

    Ui::TemplateConfigurationPage *const ui;
};
}
}
