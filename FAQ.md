# FAQ

## Issues

### Can't save a password

* Is folder initialised? Easiest way is to use the [Users] button
  and make sure you can encrypt for someone (eg. yourself)
* Are you using git? If not, make sure it is switched off.

### I don't get a passphrase / PIN dialog

* You'll need to install pinentry-qt (or -qt4 or -qt5 or even -gtk) and
  possibly set the full path to that executable in your `~/.gnupg/gpg-agent.conf`
  for example: `pinentry-program /usr/bin/pinentry-qt4`
* On some esotheric systems it might be necessary to create a symbolic
  link `/usr/bin/pinentry` to your pinentry application of choice
  eg: `ln -s /usr/bin/pinentry-qt5 /usr/bin/pinentry`
* On macOS `pinentry-program /usr/local/bin/pinentry-mac` works after installing `pinentry-mac` from homebrew.  

### Where is the configuration stored?

GnuPG Pass tries to use the native config choice for the OS it's running.

* Linux and BSD: `$HOME/.config/GnuPG/GnuPGPass.conf`
* Windows registry: `HKEY_CURRENT_USER\Software\GnuPG\GnuPGPass`

These settings can be over-ruled by a `gnupgpass.ini` file in the folder where the application resides.
So called "portable config".

There are some things to take care of when trying to sync on some systems (especially OSX, with regards to text and binary .plist files).

More information: <http://doc.qt.io/qt-5/qsettings.html#platform-specific-notes>

### Can I import from KeePass, LastPass or X?

* Yes, check [passwordstore.org/#migration](https://www.passwordstore.org/#migration)
  for more info.
