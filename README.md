# GnuPG Password manager

## Features

* Configurable shoulder surfing protection options
* Per-folder user selection for multi recipient encryption
* Multiple profiles
* Easy onboarding

## Installation

### From Source

#### Dependencies

* GnuPG Password Manager requires
  - Qt 5.15
  - Extra-CMake-Modules
  - A collection of KDE's Frameworks:
    - KWidgetsAddons
    - KIconThemes
    - KI18n
    - KConfigWidgets
    - Prison


Your GnuPG has to be set-up with a graphical pinentry when applicable, same goes for git authentication.

On most unix systems all you need is:

```sh
cmake -GNinja -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Debug -DBUILD_WITH_QT6=ON -DCMAKE_INSTALL_PREFIX=~/kde/usr  -S . build
cmake --build build && cmake --install build
source prefix.sh
```

## Using profiles

Profiles allow to group passwords. Each profile might use a different git repository and/or different gpg key.
A typical use case is to separate personal and work passwords.

> **Hint**<br>
> Instead of using different git repositories for the various profiles passwords could be synchronized with different
> branches from the same repository. Just clone the repository into the profile folders and checkout the related
> branch.

### Links of interest

* [Git Tools - Credential Storage](https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage)
* [Dealing with secrets](https://gist.github.com/maelvls/79d49740ce9208c26d6a1b10b0d95b5e)
* [Git Credential Manager](https://github.com/GitCredentialManager/git-credential-manager)
* [password-store](https://git.zx2c4.com/password-store/about/)

## Security considerations

Using this program will not magically keep your passwords secure against
compromised computers even if you use it in combination with a smartcard.

It does protect future and changed passwords though against anyone with access to
your password store only but not your keys.
Used with a smartcard it also protects against anyone just monitoring/copying
all files/keystrokes on that machine and such an attacker would only gain access
to the passwords you actually use.
Once you plug in your smartcard and enter your PIN (or due to CVE-2015-3298
even without your PIN) all your passwords available to the machine can be
decrypted by it, if there is malicious software targeted specifically against
it installed (or at least one that knows how to use a smartcard).

To get better protection out of use with a smartcard even against a targeted
attack I can think of at least two options:

* The smartcard must require explicit confirmation for each decryption operation.
  Or if it just provides a counter for decrypted data you could at least notice
  an attack afterwards, though at quite some effort on your part.
* Use a different smartcard for each (group of) key.
* If using a YubiKey or U2F module or similar that requires a "button" press for
  other authentication methods you can use one OTP/U2F enabled WebDAV account per
  password (or groups of passwords) as a quite inconvenient workaround.
  Unfortunately I do not know of any WebDAV service with OTP support except ownCloud
  (so you would have to run your own server).

## Known issues

* Filtering (searching) breaks the tree/model sometimes
* Starting without a correctly set password-store folder
  gives weird results in the tree view

## License

### GNU GPL v3.0

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

View official GNU site <http://www.gnu.org/licenses/gpl.html>.

[![OSI](http://opensource.org/trademarks/opensource/OSI-Approved-License-100x137.png)](https://opensource.org/licenses/GPL-3.0)

[View the Open Source Initiative site.](https://opensource.org/licenses/GPL-3.0)
